<?php

/**
 * @file
 * Hooks specific to the rabbit hole links module.
 */

declare(strict_types=1);

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\Core\Url;
use Drupal\rabbit_hole\Plugin\RabbitHoleBehaviorPluginInterface;

/**
 * Allows altering the rabbit_hole link behavior.
 *
 * @param \Drupal\Core\Url $url
 *   The url object.
 * @param \Drupal\rabbit_hole\Plugin\RabbitHoleBehaviorPluginInterface $rabbit_hole_behavior_plugin
 *   The rabbit_hole_behavior plugin.
 */
function hook_rabbit_hole_links_alter(Url $url, RabbitHoleBehaviorPluginInterface $rabbit_hole_behavior_plugin) {
  if (isset($rabbit_hole_behavior_settings['action']) && $rabbit_hole_behavior_settings['action'] == 'custom_action') {
    $url = Url::fromRoute('<nolink>');
  }
}

/**
 * @} End of "addtogroup hooks".
 */
