# Rabbit hole links

CONTENTS OF THIS FILE
------------

- Introduction
- Requirements
- Installation
- Configuration
- Development
- Maintainers


INTRODUCTION
------------

This module changes the behavior of links, linking to an entity using
rabbit_hole behaviors. It uses the configuration to manipulate the link to
change as follows:
- If the behavior is set to "Page not Found" or "Access Denied" it will disable
the link.
- If the behavior is set to "Redirect" the configured redirect url will be used
directly for creating the link to avoid the redirect for the user.

Rabbit hole links will respect the bypass permissions by only changing the links
behavior when the user does not have the "rabbit hole bypass [entity_type_id]"
permission.


REQUIREMENTS
------------

* Requires [Rabbit hole](https://www.drupal.org/project/rabbit_hole)


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
  See [installing modules](https://www.drupal.org/node/1897420) for further
  information.


CONFIGURATION
-------------

There is no configuration needed.


DEVELOPMENT
-----------

It is possible to integrate custom rabbit_hole behavior plugin link manipulation
by implementing `hook_rabbit_hole_links_alter`.


MAINTAINERS
-----------

Current maintainers:

* Pascal Crott - https://www.drupal.org/u/hydra
