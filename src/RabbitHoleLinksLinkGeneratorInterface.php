<?php

declare(strict_types=1);

namespace Drupal\rabbit_hole_links;

use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGeneratorInterface;

/**
 * Interface for the link generator.
 */
interface RabbitHoleLinksLinkGeneratorInterface extends LinkGeneratorInterface {

  /**
   * Alters the url object.
   *
   * @param \Drupal\Core\Url $url
   *   The url.
   *
   * @return \Drupal\Core\Url
   *   The altered url.
   */
  public function alterUrl(Url $url): Url;

}
